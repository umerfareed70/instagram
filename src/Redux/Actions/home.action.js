import * as Types from '../Types/home.types';

/////////////////////////////////////////  save Info   ////////////////////////////
export const Save_Info = (data) => async dispatch => {
  dispatch({
    type: Types.Set_Data_loader,
    payload: true,
  });
  try {
    dispatch({
      type: Types.Save_Data_Success,
      payload: data,
    });
    } catch (error) {
    dispatch({
      type: Types.Save_Data_Failure,
      payload: null,
    });
    alert('Something went wrong');
  }
};
