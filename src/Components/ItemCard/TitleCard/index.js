import React from 'react';
import {Text, View, Image} from 'react-native';
import ThreeDotMenu from '../../ThreeDotMenu';
import styles from './style';
const index = props => {
  return (
    <View style={styles.container}>
      <View style={styles.rowContainer}>
        <Image source={{uri: props.sampleImage}} style={styles.leftImage} />
        <View>
          <Text>{props.title}</Text>
          <Text>{props.description}</Text>
        </View>
      </View>
        <ThreeDotMenu />
    </View>
  );
};

export default index;
