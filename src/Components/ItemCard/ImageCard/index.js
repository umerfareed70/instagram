import React from 'react'
import {   View,Image, } from 'react-native'
import styles from "./style"
const index = (props) => {
    return (
        <View style={styles.imageContainer}>
            <Image style={styles.imageStyle} source={{uri:props.sampleImage}}/>
       </View>
    )
}

export default index

