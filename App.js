import React from 'react';
import Home from './src/Screens/Dashboard';
import {NativeBaseProvider} from 'native-base';
import {Provider} from 'react-redux';
import {persistor, store} from '../Instagram/src/Redux/Store/store';
import {PersistGate} from 'redux-persist/integration/react';

const App = () => {
  console.disableYellowBox = true;

  return (
    <NativeBaseProvider>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Home />
        </PersistGate>
      </Provider>
    </NativeBaseProvider>
  );
};
export default App;
