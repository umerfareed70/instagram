import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import styles from './styles'

const index = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Likes {props.likes}</Text>
        </View>
    )
}

export default index

