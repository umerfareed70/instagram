import React from 'react';
import {StyleSheet, Text, Image, View} from 'react-native';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
  MenuProvider,
} from 'react-native-popup-menu';
import {Images} from '../../Theme';
import styles from './styles';
const ThreeDotsMenu = props => {
  return (
   
    <MenuProvider>
      <Menu>
        <MenuTrigger>
          <View style={styles.dotStyle}>
            <Image source={Images.dots} />
          </View>
        </MenuTrigger>
        <MenuOptions style={styles.menOpt}>
          <MenuOption style={styles.men}>
            <Text>Preview</Text>
          </MenuOption>
        </MenuOptions>
      </Menu>
    </MenuProvider>
   
  );
};
export default ThreeDotsMenu;
