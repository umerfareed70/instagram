import React from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import colors from '../../Theme/Colors';
import images from '../../Theme/Images';
import MyStatusBar from '../StatusBar';
import styles from './style';
const index = ({title}) => {
  return (
    <>
      <MyStatusBar backgroundColor={colors.white} barStyle={'dark-content'} />
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.contentContainer}>
            <Text style={styles.header}>{title}</Text>
          </View>

          <TouchableOpacity>
            <Image source={images.add_Post} style={styles.icon25} />
          </TouchableOpacity>

          <TouchableOpacity>
            <Image source={images.favorite} style={styles.icon25} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image source={images.messeger} style={styles.icon25} />
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

export default index;
