import React from 'react'
import { View,SafeAreaView,StatusBar } from 'react-native'
import styles from "./style"

  const index = ({backgroundColor,barStyle},...props) => {
      return (
        <View style={[styles.statusBar, { backgroundColor }]}>
        <SafeAreaView>
          <StatusBar barStyle={barStyle} translucent backgroundColor={backgroundColor} {...props} />
        </SafeAreaView>
      </View>
      )
  }
  
  export default index
  
  
  