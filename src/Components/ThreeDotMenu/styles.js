import {StyleSheet} from 'react-native';
import colors from '../../Theme/Colors';

export default styles = StyleSheet.create({
  menOpt: {
    backgroundColor: colors.BlackOpacity_4,
  },
  men: {
    borderBottomWidth: 1,
    padding: 10,
  },
  textView: {},

  dotStyle: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginRight: 20,
    marginTop: 10,
  },
});
