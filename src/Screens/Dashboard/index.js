import React, {useState, useEffect} from 'react';
import {Container} from 'native-base';
import Header from '../../Components/Header';
import FlatCard from '../../Components/FlatCard';
import {useSelector, useDispatch} from 'react-redux';
import {Save_Info} from '../../Redux/Actions/home.action';
const index = () => {
  const dispatch = useDispatch(null);
const home = useSelector(state => state.home)
  useEffect(() => {
    dispatch(Save_Info([1, 2, 3, 4, 5, 6]));
  }, []);
  return (
    <Container>
      <Header title={'Instagram'} />
      <FlatCard postData={home?.data} />
    </Container>
  );
};

export default index;
