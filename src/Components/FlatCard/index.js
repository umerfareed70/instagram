import React from 'react';
import {Text, View, FlatList} from 'react-native';
import {sampleImage} from '../../Theme/routes';
import HeaderCard from '../HeaderCards';
import Card from '../ItemCard/Card';
import styles from './style';

const index = props => {
  return (
    <View style={styles.container}>
      <FlatList
        contentContainerStyle={styles.contentContainer}
        data={props.postData}
        ListHeaderComponent={
          <HeaderCard title={'Stories'} sampleImage={sampleImage} />
        }
        ItemSeparatorComponent={() => {
          return <View style={styles.separator} />;
        }}
        renderItem={({item}) => {
          return (
            <View>
              <Card sampleImage={sampleImage} />
            </View>
          );
        }}
      />
    </View>
  );
};

export default index;
