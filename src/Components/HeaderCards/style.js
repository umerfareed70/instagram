import { StyleSheet } from "react-native";
import colors from "../../Theme/Colors";

import {WP} from '../../Theme/responsive';
export default styles=StyleSheet.create({
    container: {
        width:WP('100'),
        backgroundColor: colors.white,
      },
      imageContainer:{
        justifyContent: 'center', alignItems: 'center'
      },
      imageStyle:{
        backgroundColor: colors.white,
        borderRadius: 60,
        height: 60,
        width: 60,
        margin: 5,
      },
      imageIcon:{
        height:'100%',borderRadius:60
      }
      ,
      textContainer:{

      },
      textStyle:{
        fontSize:12
      }

})