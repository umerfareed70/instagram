import {StyleSheet, Dimensions} from 'react-native';

export default styles = StyleSheet.create({
  imageStyle: {
    height: Dimensions.get('screen').height / 2,
    resizeMode: 'cover',

  },
  imageContainer:{
  }
});
