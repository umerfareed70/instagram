import { StyleSheet } from "react-native";
import colors from "../../Theme/Colors";

import {WP} from '../../Theme/responsive';
export default styles=StyleSheet.create({
    container: {
        height: WP('12'),
        width:WP('100'),
        backgroundColor: colors.white,
      },
      wrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: WP('4'),
        paddingVertical:WP('2')
      },
      contentContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        width:'60%'
      },
      header: {
        fontSize: WP('6'),
        fontWeight: 'bold',
        color: colors.black,
      },
      icon25:{
        height:20,width:20,resizeMode:'contain'
      }
})