import React from 'react';
import {FlatList, Text, View, Image} from 'react-native';
import styles from './style';
const index = props => {
  return (
    <View style={styles.container}>
      <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={[1, 2, 3, 4, 5, 6, 7, 8]}
        renderItem={({item}) => {
          return (
            <View style={styles.imageContainer}>
              <View style={styles.imageStyle}>
                <Image
                  source={{
                    uri: props.sampleImage,
                  }}
                  style={styles.imageIcon}
                />
              </View>
              <Text style={styles.textStyle}>{props.title}</Text>
            </View>
          );
        }}
      />
    </View>
  );
};

export default index;
