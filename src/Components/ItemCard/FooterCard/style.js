import {StyleSheet} from 'react-native';

export default styles = StyleSheet.create({
  container: {
    padding: 5,
  },
  icon25: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  imageStyle: {
    margin: 5,
  },
  h1:{
    fontWeight:'bold'
  },
  h2:{
    fontWeight:'normal'
  }
});
