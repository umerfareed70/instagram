import React from 'react';
import {Text, View, Image} from 'react-native';
import Images from '../../../Theme/Images';
import styles from './style';
import LikeCard from './LikeCard';
const index = props => {
  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row'}}>
        <View style={styles.imageStyle}>
          <Image source={Images.add_Post} style={styles.icon25} />
        </View>

        <View style={styles.imageStyle}>
          <Image source={Images.favorite} style={styles.icon25} />
        </View>
        <View style={styles.imageStyle}>
          <Image source={Images.messeger} style={styles.icon25} />
        </View>
      </View>
      <View>
        <LikeCard likes={'10'} />
        <Text style={styles.h1}>
          Lorem{' '}
          <Text style={styles.h2}>
            Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has been the industry's standard dummy text ever since
            the 1500s, when an unknown printer took a galley of type and
            scrambled it to make a type specimen book. It has survived not only
            five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised in the 1960s
            with the release of Letraset sheets containing Lorem Ipsum passages,
            and more recently with desktop publishing software like Aldus
            PageMaker including versions of Lorem Ipsum.
          </Text>
        </Text>
      </View>
    </View>
  );
};

export default index;
