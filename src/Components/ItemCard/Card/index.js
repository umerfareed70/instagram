import React from 'react';
import {View} from 'react-native';
import TitleCard from '../TitleCard';
import ImageCard from '../ImageCard';
import FooterCard from '../FooterCard';
import VideoCard from '../VideoCard';

import styles from './style';
const index = props => {
  return (
    <>
      <View style={styles.container}>
        <TitleCard
          title={'#developer'}
          description={'Have a  nice day'}
          sampleImage={props.sampleImage}
        />
      </View>
      <ImageCard sampleImage={props.sampleImage} />
      {props?.video && <VideoCard />}
      <View style={styles.container}>
        <FooterCard />
      </View>
    </>
  );
};

export default index;
