import { StyleSheet } from "react-native";

export default  styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        paddingVertical: 10,
        },
        rowContainer:{
            width: '70%', flexDirection: 'row'
        },
        leftImage:{
            height: 30, width: 30, borderRadius: 30, marginRight: 4
        },
      
    
})