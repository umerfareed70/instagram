import * as Types from '../Types/home.types';
const initialState = {
  loading: false,
  success: false,
  failure: false,
  data: [],
};
export default (state = initialState, action = {}) => {
  const {type, payload} = action;
  switch (type) {
    case Types.Save_Data_Success:
      return {
        ...state,
        loading: false,
        success: true,
        failure: false,
        data: payload,
      };

    case Types.Save_Data_Success:
      return {
        ...state,
        loading: false,
        success: false,
        failure: true,
        data: null,
      };
    case Types.Set_Pawtai_loader:
      return {
        ...state,
        loading: payload,
      };

    default:
      return state;
  }
};
