import {StyleSheet} from 'react-native';

export default styles = StyleSheet.create({
  container: {
    paddingVertical: 5,
  },
  text: {
    fontWeight: 'bold',
  },
});
