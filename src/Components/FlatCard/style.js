import {StyleSheet} from 'react-native';
import colors from '../../Theme/Colors';

import {WP} from '../../Theme/responsive';
export default styles = StyleSheet.create({
  container: {
    width: WP('100'),
    height: WP('200'),
    backgroundColor: colors.white,
  },
  separator: {
    borderWidth: 0.5,
    borderColor: colors.input_light_gray,
    marginVertical: 5,
  },
  contentContainer:{
    paddingBottom:50
  }
});
